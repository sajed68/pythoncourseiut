### IUT ANN Course 97-2


<p dir=rtl>
در این مخزن فایل‌های مورد نیاز برای درس شبکه عصبی در اختیار دانشجویان قرار می‌گیرد.
</p>
<p dir=rtl>
لینک ویدیوها در آپارات:
</p>  

> [https://www.aparat.com/SajedRakhsani](https://www.aparat.com/SajedRakhsani)  
> (بله اسم اشتباه نوشته شده :) )  

> توجه: اگر احتمالا وقتی از گزینه دانلود سبز رنگ در گوشه بالا سمت راست استفاده می‌کنید، ممکن هست ویدیوها بارگیری نشوند، بهتر هست ویدیو‌ها را یکی یکی از اینجا و یا آپارات بگیرید. ضمن این که گیت‌هاب اجازه بارگزاری ویدیوی بیشتر را به من نمی‌دهد!  


          
* [نگاه اولیه به ژوپیتر](https://gitlab.com/sajed68/pythoncourseiut/tree/master/Jupyter%20for%20Beginners/NoteBook_Tutorial.ipynb)  
* [جلسه اول-اعداد](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture01)  
* [جلسه دوم-رشته‌ها](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture02)  
* [جلسه سوم-لیست و چندتایی](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture03)
* [جلسه چهارم-گزاره‌های شرطی](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture04)  
* [جلسه پنجم-حلقه‌ها۱](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture05)  
* [جلسه ششم-حلقه‌ها۲](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture06)  
* [جلسه هفتم-حلقه‌ها ۳](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture07)  
* [جلسه هشتم-تمرین پایتونی-حدس عدد](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture08)  
* [جلسه نهم-توابع و متدها](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture09)  
* [جلسه دهم-ماژول‌ها و بسته‌ها](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture10)  
* [جلسه یازدهم-مقدمه‌ای بر نامپی](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture11)  
* [تمرین کامپیوتری سری اول- متلب](https://gitlab.com/sajed68/pythoncourseiut/tree/master/CHW01)  
* [جلسه دوازدهم- شروع یک مثال ساده:پرسپترون تک لایه](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture12)  
* [جلسه دوازدهم- شروع یک مثال ساده:ابزار scikit-learn](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture12)  
* [جلسه سیزدهم- خلاصه مطالب، تعریف پروژه نهایی و تمرین کامپیوتری دوم](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture13)  
* [جلسه چهاردهم- مقدمه‌ای برای Keras](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture14)  
* [جلسه پانزدهم- یک نمونه تمرین](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture15)  
* [جلسه شانزدهم- آشنایی با سامانه محاسباتی گوگل](https://gitlab.com/sajed68/pythoncourseiut/tree/master/lecture16)  




-------------------------
در جدول زیر محتوای ویدیو هر جلسه آمده است. 


| زمان        | محتوا           | جلسه  |
| ------------- |:-------------:| -----:|
|      ۲۳ دقیقه         |مقدمه- اعداد در پایتون | جلسه اول |  
|۱۶ دقیقه | رشته‌ها | جلسه دوم |  
| ۱۲ دقیقه | لیست‌ها و چندتایی‌ها | جلسه سوم| 
|۲۰ دقیقه | عملگرهای مقایسه‌ای و گزاره‌های شرطی| جلسه چهارم|  
|۱۶ دقیقه |حلقه‌های for | جلسه پنجم |  
|۱۱ دقیقه | حلقه‌های While | جلسه ششم |  
|۱۰ دقیقه | چند دستور پرکاربرد در مورد حلقه‌ها| جلسه هفتم|  
|۴ دقیقه | تمرین اول-صورت سوال- حدس عدد| جلسه هشتم|  
|۴۴ دقیقه | توابع و متدها | جلسه نهم|  
|۱۶ دقیقه | ماژول و  بسته در پایتون| جلسه دهم|  
|۵۱ دقیقه| مقدمه‌ای بر Numpy| جلسه یازدهم|  
|۳۷ دقیقه| مثال ساده-نامپی- بخش ۱ | جلسه دوازدهم|  
|۱۳ دقیقه| مثال ساده- sklearn-بخش ۲|جلسه دوازدهم|  
|۵۱ دقیقه| خلاصه مطالب، توضیح درمورد پروژه-بخش ۱|جلسه سیزدهم|  
|۵۲ دقیقه| توضیح درمورد تمرین و پروژه - بخش ۲| جلسه سیزدهم|  
|۵۱ دقیقه| آشنایی با Keras- بخش ۱| جلسه چهاردهم|  
|۳۲ دقیقه| آشنایی با Keras-بخش ۲| جلسه چهاردهم|  
|۲۵ دقیقه| مرور چند تمرین | جلسه پانزدهم|  
|۱۰ دقیقه|آشنایی با سامانه محاسباتی گوگل برای شبیه‌سازی پروژه‌ها| جلسه شانزدهم|  








[![CC-BY-NC-ND](https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)
